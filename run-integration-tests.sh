# This config refers to services set up in the docker-compose file.
# For now, these will need to be kept in sync with the .gitlab-ci.yml file.

export QUERY_ELASTIC_SHARD_COUNT=10
export GLOBAL_EVENT_BUS_BASE=https://bus.eventdata.crossref.org
export QUERY_DEPLOYMENT=test_
export GLOBAL_ARTIFACT_URL_BASE=https://artifact.eventdata.crossref.org
export QUERY_EVENT_BUS_BASE=https://bus.eventdata.crossref.org
export QUERY_PORT=8100
export QUERY_JWT=TEST
export GLOBAL_JWT_SECRETS=TEST,TEST2
export QUERY_WHITELIST_OVERRIDE=true
export QUERY_TERMS_URL=https://doi.org/10.13003/CED-terms-of-use
export QUERY_PREFIX_WHITELIST_ARTIFACT_NAME=crossref-doi-prefix-list
export QUERY_ELASTIC_URI=http://localhost:9200

docker-compose -f docker-compose-integration.yml up -d

lein test :integration
RESULT=$?

docker-compose -f docker-compose-integration.yml down
exit $RESULT