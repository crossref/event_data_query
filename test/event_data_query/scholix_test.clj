(ns event-data-query.scholix-test
  (:require [clojure.test :refer :all]
            [event-data-query.scholix :as scholix]
            [clj-time.core :as clj-time]))

(deftest ^:unit should-index-document
  (testing "should-index-document returns a boolean so it's suitable for Elastic bool field"
           (is (= true (scholix/should-index-document "literature" "dataset"))
               "Should be true for literature to dataset event")
           (is (= true (scholix/should-index-document "dataset" "literature"))
               "Should be true for dataset to literature event")
           (is (= false (scholix/should-index-document "literature" "literature"))
               "Should be false for link between two literature items")
           (is (= false (scholix/should-index-document "dataset" "dataset"))
               "Should be false for link between two dataset items")

           (is (= false (scholix/should-index-document "literature" ""))
               "Should be false if type missing")

           (is (= false (scholix/should-index-document "literature" nil))
               "Should be false if type missing")

           (is (= false (scholix/should-index-document "" "dataset"))
               "Should be false if type missing")

           (is (= false (scholix/should-index-document nil "dataset"))
               "Should be false if type missing")))

(deftest ^:unit include-data-citation
  (let [; A base ElasticSearch document that's not relevant to Scholix.
        document {:subj-id "https://doi.org/10.5555/1"
                  :obj-id "https://doi.org/10.5555/1"
                  :subj-content-type "other"
                  :obj-content-type "other"
                  :relation-type "references"}]

    (testing "Scholix Events should not be included if they are not article-data links (other to other)."
      (let [document (merge document {:subj-content-type "other"
                                      :obj-content-type "other"})]
        (is (nil? (scholix/document->event document))
            "other -> other should be excluded.")))

    (testing "Scholix Events should not be included if they are not article-data links (other to dataset)."
      (let [document (merge document {:subj-content-type "other"
                                      :obj-content-type "dataset"})]
        (is (nil? (scholix/document->event document))
            "other -> dataset should be excluded.")))

    (testing "Scholix Events should not be included if they are not article-data links (dataset to other)."
      (let [document (merge document {:subj-content-type "dataset"
                                      :obj-content-type "other"})]
        (is (nil? (scholix/document->event document))
            "dataset -> other should be excluded.")))

    (testing "Scholix Events should not be included if they are not article-data links (dataset to dataset)."
      (let [document (merge document {:subj-content-type "dataset"
                                      :obj-content-type "dataset"})]
        (is (nil? (scholix/document->event document))
            "dataset -> dataset should be excluded.")))

    (testing "Scholix Events should not be included if they are not article-data links (journal_article to journal_article)."
      (let [document (merge document {:subj-content-type "journal_article"
                                      :obj-content-type "journal_article"})]
        (is (nil? (scholix/document->event document))
            "literature -> literature should be excluded.")))

    ; Positive!

    (testing "Scholix Events should be included if they are not article-data links (journal_article to dataset)."
      (let [document (merge document {:subj-content-type "journal_article"
                                      :obj-content-type "dataset"})]
        (is (scholix/document->event document)
            "article -> dataset should be included.")))

    (testing "Scholix Events should be included if they are not article-data links (dataset to journal_article)."
      (let [document (merge document {:subj-content-type "dataset"
                                      :obj-content-type "journal_article"})]
        (is (scholix/document->event document)
            "dataset -> article should be included.")))))
  
  
