(ns event-data-query.server-test
  (:require [clojure.test :refer :all]
            [event-data-query.server :as server]
            [ring.mock.request :as mock]
            [ring.middleware.params :as params]))

(deftest ^:unit try-parse-int
  (testing "try-parse-int can parse valid integer"
    (is (= 12345 (server/try-parse-int "12345")))
    (is (thrown? NumberFormatException (server/try-parse-int "ONE TWO THREE FOUR")))))

(deftest ^:unit document->event
  (testing "document->event adds extra fields"
           ; Normally comes from environment variable.
           (with-redefs [server/terms-url "https://doi.org/10.13003/CED-terms-of-use"]
             (is (= (server/document->event {:event {"input" "event"}})
                    {"input" "event"
                     "terms" "https://doi.org/10.13003/CED-terms-of-use"})))))

(deftest ^:unit num-rows-validation
  (testing "rows within limits (0 <= rows <= max-page-size)"
    (with-redefs [server/handle-ok (fn [_ _ _ _ _] "ok")]
      
      (is (= ((server/query-events :standard document->event "event-list" "events" {})
              (params/params-request (mock/request :get "/event" {:rows 0})))
             {:status 200, :headers {"Content-Type" "application/json;charset=UTF-8", "Vary" "Accept"}, :body "ok"})
          "Asking for 0 rows should be valid")
      
      (is (= ((server/query-events :standard document->event "event-list" "events" {})
              (params/params-request (mock/request :get "/event" {:rows 1})))
             {:status 200, :headers {"Content-Type" "application/json;charset=UTF-8", "Vary" "Accept"}, :body "ok"})
          "Asking for 0 < rows <= max-page-size should be valid")
      
      (is (= ((server/query-events :standard document->event "event-list" "events" {})
              (params/params-request (mock/request :get "/event" {:rows server/max-page-size})))
             {:status 200, :headers {"Content-Type" "application/json;charset=UTF-8", "Vary" "Accept"}, :body "ok"})
          "Asking for rows == max-page-size should be valid")))
  
  (testing "rows out of bounds (rows < 0 || rows > max-page-size)"    
      (is (= 400 (:status ((server/query-events :standard document->event "event-list" "events" {})
                       (params/params-request (mock/request :get "/events" {:rows -1})))))
          "Asking for 0 rows should be valid")

      (is (= 400 (:status ((server/query-events :standard document->event "event-list" "events" {})
                       (params/params-request (mock/request :get "/events" {:rows (+ server/max-page-size 1)})))))
          "Asking for 0 < rows <= max-page-size should be valid")))

(deftest ^:unit num-rows-unspecified
  (is (= server/max-page-size (server/get-rows {}))
      "when not specified the number of rows must be equal to max-page-size"))