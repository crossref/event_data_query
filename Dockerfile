# Event Data Query API

FROM clojure:openjdk-11-lein-2.9.3
MAINTAINER Joe Wass jwass@crossref.org

COPY target/uberjar/*-standalone.jar ./query-api.jar

ENTRYPOINT ["java", "-jar", "query-api.jar"]
