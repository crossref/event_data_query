; This version number, and resources/VERSION.txt are set in the CI build.
(defproject event-data-query "0.1.0-SNAPSHOT"
  :description "Event Data Query"
  :url "http://eventdata.crossref.org/"
  :license {:name "MIT License"
            :url "https://opensource.org/licenses/MIT"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [org.clojure/core.cache "0.7.1"]
                 [org.clojars.ppandis/event-data-common "0.2.2"]
                 [org.clojure/data.json "0.2.6"]
                 [crossref-util "0.1.15"]
                 [clj-http "3.4.1"]
                 [clj-http-fake "1.0.3"]
                 [ring/ring-mock "0.3.0"]
                 [overtone/at-at "1.2.0"]
                 [robert/bruce "0.8.0"]
                 [yogthos/config "0.8"]
                 [clj-time "0.12.2"]
                 [org.apache.httpcomponents/httpclient "4.5.2"]
                 [org.apache.commons/commons-io "1.3.2"]
                 [org.clojure/tools.logging "1.2.4"]
                 [clojurewerkz/quartzite "2.0.0"]
                 [slingshot "0.12.2"]
                 [cc.qbits/spandex "0.7.6"]
                 [compojure "1.5.1"]
                 [liberator "0.15.3"]
                 [ring "1.5.0"]
                 [ring/ring-servlet "1.5.0"]
                 [ring-logger "1.0.1"]
                 [com.climate/claypoole "1.1.4"]
                 [camel-snake-kebab "0.4.0"]
                 [org.clojure/core.async "0.4.474"]

                 ; java 11 has removed EE components
                 ; These need to be imported manually now
                 [javax.xml.bind/jaxb-api "2.3.1"]]

  :main ^:skip-aot event-data-query.core
  :plugins [[lein-cljfmt "0.5.7"]]
  ; This is required so that dependencies can be cached in CI.
  :local-repo ".local-m2"
  :target-path "target/%s"
  :jvm-opts ["-Duser.timezone=UTC" "-Xmx1G" "-XX:-OmitStackTraceInFastThrow"]
  :profiles {:uberjar {:aot :all}}
  :test-selectors {:default (constantly true)
                   :unit :unit
                   :component :component
                   :integration :integration
                   :all (constantly true)})
